package com.example.sukhrana.androidgalaga;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class ResultActivity extends AppCompatActivity {

    ImageView image;
    String s;
    MediaPlayer win;
    MediaPlayer loss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
         Intent i = getIntent();
        win = MediaPlayer.create(this, R.raw.win);
        loss = MediaPlayer.create(this, R.raw.loss);

        image = (ImageView) findViewById(R.id.imageView3);
         this. s = i.getStringExtra("lives");
         if(this.s.equals("l")) {

             image.setImageResource(R.drawable.lose);
             loss.start();
         }
         else{
             image.setImageResource(R.drawable.win);
             win.start();
         }


    }

    public void play(View view) {
        Intent s = new Intent(this, MainActivity.class);
        startActivity(s);
        loss.release();
        win.release();
    }
}
