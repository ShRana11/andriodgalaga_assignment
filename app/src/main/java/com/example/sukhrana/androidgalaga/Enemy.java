package com.example.sukhrana.androidgalaga;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Enemy {

    Bitmap Image;
    Rect Hitbox;
    int xPosition;
    int yPosition;
    int pos;

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }

    public Rect getHitbox() {
        return Hitbox;
    }

    public void setHitbox(Rect hitbox) {
        Hitbox = hitbox;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public  Enemy(Context context, int x, int  y, int i){
        this.xPosition = x;
        this.yPosition = y;
        this.pos = i;

        //set default image- allenemy have samne image and hitbox
        if(i == 1) {
            this.Image = BitmapFactory.decodeResource(context.getResources(), R.drawable.macaw);
        }else if (i == 2){
            this.Image = BitmapFactory.decodeResource(context.getResources(), R.drawable.toucan);
        }
       // this.Image.setWidth(this.Image.getWidth()/2);

        // version 1 - we use static numbers because the enemy is NOT moving.
        this.Hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.Image.getWidth(),
                this.yPosition + this.Image.getHeight()
        );
        //set the default hitbox

        //getter sand setter



    }

}

