package com.example.sukhrana.androidgalaga;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;

public class MainActivity extends AppCompatActivity {

private GameEngine gameEngine;

        // screen size variables
        Display display;
        Point size;
        int screenHeight;
        int screenWidth;
        Intent i;


@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         Intent myIntent = new Intent(this,ResultActivity.class);


        display = getWindowManager().getDefaultDisplay();
        size = new Point();

        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;
        i = myIntent;

        gameEngine = new GameEngine(this, screenWidth, screenHeight, i);
        setContentView(gameEngine);
        }


@Override
protected void onPause() {
        super.onPause();
        gameEngine.pauseGame();
        }

@Override
protected void onResume() {
        super.onResume();
        gameEngine.resumeGame();
        }


        }

