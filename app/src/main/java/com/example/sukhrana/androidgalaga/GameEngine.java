package com.example.sukhrana.androidgalaga;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    private final String TAG = "BSBSBSBS";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;
    Enemy selectedkamakazi;
    int kamakazi = 0;
    int checkminutes;
    int score = 0;
    Intent myIntent;
    boolean youWin = false;
    boolean time = false;
    boolean drag = false;
    int deltaX;
    int deltaY;
    Rect bombHitBox;
    Point bombPoint;
    MediaPlayer mysong;
    MediaPlayer bombShow;
    MediaPlayer blast;
    MediaPlayer dead;
    int tripleLive = 9;


    //myIntent.putExtra("key", value); //Optional parameters
    //CurrentActivity.this.startActivity(myIntent);


    // Screen resolution varaibles

    private int screenWidth;
    private int screenHeight;
    Context c;

    int lives = 3;
    Player monkey;
    Bitmap bc1;
    Bitmap bc2;
    Bitmap bomb;

    int SQUARE_WIDTH = 10;
    List<Sprite> bullets = new ArrayList<Sprite>();
    List<Eggs> egg = new ArrayList<Eggs>();
    List<Enemy> ene = new ArrayList<Enemy>();
    //List<Enemy> ene1 = new ArrayList<Enemy>();
    Point pic1;
    Point pic2;

    public GameEngine(Context context, int screenW, int screenH, Intent myIntent) {
        super(context);

        this.c = context;

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.myIntent = myIntent;
        mysong = MediaPlayer.create(context, R.raw.n);
        bombShow = MediaPlayer.create(context,R.raw.bomb);
        blast = MediaPlayer.create(context, R.raw.bomb1);
        dead = MediaPlayer.create(context, R.raw.dead);




        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;


        //monkey bullets


        this.bullets.add(new Sprite(context, 100, 1400));
        bullets.add(new Sprite(context, 100, 900));
        bullets.add(new Sprite(context, 100, 400));
        bullets.add(new Sprite(context, 100, -100));
        bullets.add(new Sprite(context, 100, -600));
        bullets.add(new Sprite(context, 100, -1100));


        this.monkey = new Player(context, 100, screenHeight- 420);
        this.bomb = BitmapFactory.decodeResource(getResources(), R.drawable.bonfire);



        //enemy bullets

        this.egg.add(new Eggs(context, this.screenWidth - 500, 200));


        this.ene.add(new Enemy(context, this.screenWidth - 200, 200, 1));
        this.ene.add(new Enemy(context, this.screenWidth - 400, 200, 1));
        this.ene.add(new Enemy(context, this.screenWidth - 600, 200, 1));
        this.ene.add(new Enemy(context, this.screenWidth - 800, 200, 1));
        //this.ene.add(new Enemy(context,this.screenWidth - 200, 200,1));

        this.ene.add(new Enemy(context, this.screenWidth - 200, 500, 2));
        this.ene.add(new Enemy(context, this.screenWidth - 400, 500, 2));
        this.ene.add(new Enemy(context, this.screenWidth - 600, 500, 2));
        this.ene.add(new Enemy(context, this.screenWidth - 800, 500, 2));
        //this.ene.add(new Enemy(context,xAxis, yAxis,2));

        this.pic1 = new Point();
        this.pic1.x = 0;
        this.pic1.y = 0;

        this.pic2 = new Point();
        this.pic2.x = 0;
        this.pic2.y = screenHeight;

        this.bc1 = BitmapFactory.decodeResource(getResources(), R.drawable.pic);
        this.bc2 = BitmapFactory.decodeResource(getResources(), R.drawable.pic);
        this.bombPoint = new Point();
        bombPoint.x = 800;
        bombPoint.y = screenHeight- 420;

        this.bombHitBox =new Rect(
                               bombPoint.x,
                               bombPoint.y ,
                               bombPoint.x+this.bomb.getWidth(),
                               bombPoint.y + bomb.getHeight());



    }

    @Override
    public void run() {
        // @TODO: Put game loop in here
        while (gameIsRunning == true) {
            updateGame();
            redrawSprites();
            controlFPS();
        }
    }

    // Game Loop methods
    int x = 10;
    boolean gameOver = false;
    boolean enemyMovingRight = false;
    int h = 200;
    int press = 0;



    public void updateGame() {
        mysong.start();

        //For Background


        pic1.y = pic1.y + 10;
        pic2.y = -bc2.getHeight() + x;
         x = x+ 10;
         if( pic2.y >= screenWidth){
             x = 10;
             pic2.y = -bc2.getHeight() + x;
         }
         if(pic1.y >= bc1.getHeight()){
             pic1.y = 0;
             pic1.y = pic1.y + 10;

         }
         //for bomb

        if(this.time == true && this.bombHitBox.intersect(this.monkey.Hitbox)){
            blast.start();
            bombShow.release();

            for(int j = 0; j < ene.size() - 4; j++){
                this.score = this.score + 1;

                ene.get(j).setxPosition(-1500);
                ene.get(j).setyPosition(-1500);
                ene.get(j).getHitbox().left = -1500;
                ene.get(j).getHitbox().top = -1500;
                ene.get(j).getHitbox().right = ene.get(j).getxPosition() + ene.get(j).getImage().getWidth();
                ene.get(j).getHitbox().bottom = ene.get(j).getyPosition() + ene.get(j).getImage().getHeight();

            }
            this.time = false;
        }

         // For Bullets

        for (int i = 0; i < bullets.size(); i++) {
            // 0. Get the bullet out of the array
            Sprite bull = bullets.get(i);


            // 1. calculate distance between bullet and enemy
            double a = 0;
            double b = 0 - this.monkey.getyPosition();

            // d = sqrt(a^2 + b^2)

            double d = Math.sqrt((a * a) + (b * b));

            Log.d(TAG, "Distance to enemy: " + d);

            // 2. calculate xn and yn constants
            // (amount of x to move, amount of y to move)
            double xn = (a / d);
            double yn = (b / d);

            // 3. calculate new (x,y) coordinates

            bull.setxPosition(bull.getxPosition() + (int) (xn * 15));
            bull.setyPosition(bull.getyPosition() + (int) (yn * 15));


            bull.getHitbox().left = bull.getxPosition();
            bull.getHitbox().top = bull.getyPosition();
            bull.getHitbox().right = bull.getxPosition() + bull.getImage().getWidth();
            bull.getHitbox().bottom = bull.getyPosition() + bull.getImage().getHeight();

            // @TODO: Collision detection between player and enemy

          for(int j = 0; j < ene.size(); j++){
            if (this.ene.get(j).getHitbox().intersect(bull.getHitbox())) {
                Log.d("ENEMYBULL", "COLLISION!!!!!");
                this.score = this.score + 1;

                ene.get(j).setxPosition(-1500);
                ene.get(j).setyPosition(-1500);
                ene.get(j).getHitbox().left = -1500;
                ene.get(j).getHitbox().top = -1500;
                ene.get(j).getHitbox().right = ene.get(j).getxPosition() + ene.get(j).getImage().getWidth();
                ene.get(j).getHitbox().bottom = ene.get(j).getyPosition() + ene.get(j).getImage().getHeight();


                //ene.get(i).getHitbox().right = -1500;
                //ene.get(i).getHitbox().left = -1500;

                Log.d(TAG, "Lives remaining: " + this.lives);


                // decide if you should be game over:
                if (this.lives == 0) {
                    this.gameOver = true;
                    return;
                }
            }
            }
            if (bull.getyPosition() <= 0) {
                bull.setxPosition(this.monkey.getxPosition());
                bull.setyPosition(this.monkey.getyPosition());
            }
        }


        //For birds and Eggs

        int previousPosx = ene.get(0).xPosition;
        int previousPosy = ene.get(0).yPosition;


        Log.d(TAG, "----------");
        for (int i = 0; i < ene.size(); i++) {
            // 0. Get the bullet out of the array
            Enemy e = ene.get(i);
            Eggs eg = egg.get(0);



            // 1. calculate distance between bullet and enemy
            double a = previousPosx - 200;
            double b = 200 - ene.get(i).yPosition;
            //for eggs
            double a1 = this.monkey.xPosition - this.ene.get(i).getxPosition();
            double  b1 = this.monkey.yPosition - this.ene.get(i).getyPosition();

            // d = sqrt(a^2 + b^2)

            double d = Math.sqrt((a * a) + (b * b));
            double d1 = Math.sqrt((a1 * a1) + (b1 * b1));

            Log.d(TAG, "Distance to enemy: " + d);

            // 2. calculate xn and yn constants
            // (amount of x to move, amount of y to move)
            double xn = (a / d);
            double yn = (b / d);

            double xn1 = (a1 / d1);
            double yn1 = (b1 / d1);

            // 3. calculate new (x,y) coordinates

            e.setxPosition(e.getxPosition() + (int) (xn * 15));
           // e.setyPosition(e.getyPosition() + (int) (yn * 15));
            eg.setyPosition(eg.getyPosition() + (int) (yn1 *15));


            e.getHitbox().left = e.getxPosition();
            e.getHitbox().top = e.getyPosition();
            e.getHitbox().right = e.getxPosition() + e.getImage().getWidth();
            e.getHitbox().bottom = e.getyPosition() + e.getImage().getHeight();


            eg.getHitbox().left = eg.getxPosition();
            eg.getHitbox().top = eg.getyPosition();
            eg.getHitbox().right = eg.getxPosition() + eg.getImage().getWidth();
            eg.getHitbox().bottom = eg.getyPosition() + eg.getImage().getHeight();


            if (eg.getyPosition() >= 1700) {
                            eg.setxPosition(this.ene.get(i).getxPosition());
                            eg.setyPosition(this.ene.get(i).getyPosition());
            }

            if (this.monkey.getHitbox().intersect(eg.getHitbox())) {
                        Log.d("MONKEYEGG", "COLLISION!!!!!");
                        this.tripleLive = tripleLive - 1;
                        if(tripleLive == 6|| tripleLive == 3|| tripleLive == 0) {
                            this.lives = this.lives - 1;
                        }
                        eg.setxPosition(this.ene.get(i).getxPosition());
                        eg.setyPosition(this.ene.get(i).getyPosition());

                        eg.getHitbox().left = eg.getxPosition();
                        eg.getHitbox().top = eg.getyPosition();
                        eg.getHitbox().right = eg.getxPosition() + eg.getImage().getWidth();
                        eg.getHitbox().bottom = eg.getyPosition() + eg.getImage().getHeight();
                        Log.d(TAG, "Lives remaining: " + this.lives);

                        // decide if you should be game over:
                        if (this.lives == 0) {
                            this.gameOver = true;
                            return;
                        }

            }



                if (e.getxPosition() <= 0) {

                    if(e.getPos() == 1){
                        previousPosy = 200;
                    }else{
                        previousPosy = 500;
                    }
                    e.setxPosition(e.getxPosition() + h);
                     h = h+ 200;
                     if(h >= screenWidth){
                         h = 0;
                     }


                    e.setyPosition(previousPosy);
                }
            if (e.getxPosition() >= screenWidth) {
                if(e.getPos() == 1){
                    previousPosy = 200;
                }else{
                    previousPosy = 500;
                }
                e.setxPosition(screenWidth - e.getxPosition());
                e.setyPosition(previousPosy);
            }
        }


        if(press == 0) {
            if(kamakazi == 0) {
                Random s = new Random();
                selectedkamakazi = ene.get(s.nextInt(ene.size()));
                kamakazi = 1;
            }
            double a = this.monkey.getxPosition() - selectedkamakazi.getxPosition();
            double b = 1200 - selectedkamakazi.getyPosition();

            // d = sqrt(a^2 + b^2)

            double d = Math.sqrt((a * a) + (b * b));

            Log.d(TAG, "Distance to cage: " + d);

            // 2. calculate xn and yn constants
            // (amount of x to move, amount of y to move)
            double xn = (a / d);
            double yn = (b / d);

            // 3. calculate new (x,y) coordinates
           int newX = selectedkamakazi.getxPosition() + (int) (xn * 30);
           int newY = selectedkamakazi.getyPosition() + (int) (yn * 30);
            selectedkamakazi.setxPosition(newX);
            selectedkamakazi.setyPosition(newY);

        }
        // @TODO: Collision detection between player and enemy
        if(selectedkamakazi.getyPosition() >= 1100){
            press = 1;
            kamakazi = 0;
           int newY1 = selectedkamakazi.getyPosition() + 40;
            selectedkamakazi.setyPosition(newY1);
            //this.bullets.setyPosition(newY1);
           // this.bullets.updateHitbox();
            if (this.monkey.getHitbox().intersect(selectedkamakazi.getHitbox())) {
                selectedkamakazi.setxPosition(-1600);
                selectedkamakazi.setyPosition(-1600);
                Log.d("MONKEYBIRD", "COLLISION!!!!!");
                this.tripleLive = tripleLive - 1;
                if(tripleLive == 6|| tripleLive == 3|| tripleLive == 0) {
                    this.lives = this.lives - 1;

                }
                 press = 0;

                selectedkamakazi.getHitbox().left = selectedkamakazi.getxPosition();
                selectedkamakazi.getHitbox().top = selectedkamakazi.getyPosition();
                selectedkamakazi.getHitbox().right = selectedkamakazi.getxPosition() + selectedkamakazi.getImage().getWidth();
                selectedkamakazi.getHitbox().bottom = selectedkamakazi.getyPosition() + selectedkamakazi.getImage().getHeight();
                Log.d(TAG, "Lives remaining: " + this.lives);

                // decide if you should be game over:
                if (this.lives == 0) {
                    this.gameOver = true;
                    return;
                }

            } else if (selectedkamakazi.getyPosition() >= 1700){
                selectedkamakazi.setxPosition(-1500);
                selectedkamakazi.setyPosition(-1500);
                press = 0;
                this.score = this.score + 1;

                selectedkamakazi.getHitbox().left = selectedkamakazi.getxPosition();
                selectedkamakazi.getHitbox().top = selectedkamakazi.getyPosition();
                selectedkamakazi.getHitbox().right = selectedkamakazi.getxPosition() + selectedkamakazi.getImage().getWidth();
                selectedkamakazi.getHitbox().bottom = selectedkamakazi.getyPosition() + selectedkamakazi.getImage().getHeight();


            }
        }


        this.monkey.getHitbox().left = this.monkey.getxPosition();
        this.monkey.getHitbox().top = this.monkey.getyPosition();
        this.monkey.getHitbox().right = this.monkey.getxPosition() + this.monkey.getImage().getWidth();
        this.monkey.getHitbox().bottom = this.monkey.getyPosition() + this.monkey.getImage().getHeight();



    }

    // boolean touch = false;

    public void touch(double x){
        if (x < this.screenWidth/2 ){
            if(monkey.getxPosition() >20) {
                this.monkey.setxPosition(monkey.getxPosition() - 40);
               // touch = false;
            } else{
                this.monkey.setxPosition(monkey.getxPosition());


            }

        }
        if(x > this.screenWidth/2){
            if(monkey.getxPosition()  < screenWidth-170) {
                this.monkey.setxPosition(monkey.getxPosition() + 40);
               // touch = false;
            } else{
                this.monkey.setxPosition(monkey.getxPosition());


            }
        }





    }

    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();

            // --------------------------------
            // @TODO: put your drawing code in this section



            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(8);

            // draw bullet
            paintbrush.setColor(Color.BLACK);
          //  canvas.drawBitmap(this.bc,0,0, bc.getWidth(),bc.getHeight(), paintbrush);



            canvas.drawBitmap(bc1, pic1.x,pic1.y,  paintbrush);
            canvas.drawBitmap(bc2, pic2.x,pic2.y,  paintbrush);





//            bc.setWidth(screenWidth);
            for (int i = 0; i < ene.size(); i++) {
                Enemy b = this.ene.get(i);
                canvas.drawBitmap(b.getImage(),b.getxPosition(),b.getyPosition(),paintbrush);
//                canvas.drawRect(
//                        b.getHitbox().left,
//                        b.getHitbox().top,
//                        b.getHitbox().right,
//                        b.getHitbox().bottom,
//                        paintbrush
//                );
            }


            for (int i = 0; i < bullets.size(); i++) {
                Sprite b = this.bullets.get(i);
               canvas.drawBitmap(b.getImage(),b.getxPosition(),b.getyPosition(),paintbrush);
//               canvas.drawRect(
//                       b.getHitbox().left,
//                      b.getHitbox().top,
//                    b.getHitbox().right,
//                    b.getHitbox().bottom,
//                    paintbrush
//               );
            }
            for (int i = 0; i < egg.size(); i++) {
                Eggs b = this.egg.get(i);
                canvas.drawBitmap(b.getImage(),b.getxPosition(),b.getyPosition(),paintbrush);
//                canvas.drawRect(
//                        b.getHitbox().left,
//                        b.getHitbox().top,
//                        b.getHitbox().right,
//                        b.getHitbox().bottom,
//                        paintbrush
//                );
            }




            // draw enemy
            paintbrush.setColor(Color.MAGENTA);


            canvas.drawBitmap(this.monkey.getImage(), this.monkey.getxPosition(), this.monkey.getyPosition(), paintbrush);

//            canvas.drawRect(
//                    this.monkey.getHitbox().left,
//                    this.monkey.getHitbox().top,
//                    monkey.getHitbox().right,
//                    monkey.getHitbox().bottom,
//                    paintbrush
//            );
            if(tripleLive == 2||tripleLive== 5|| tripleLive == 8) {

                DashPathEffect dashPath = new DashPathEffect(new float[]{0, 0}, (float) 0.0);
                // Paint p = new Paint();
                paintbrush.setColor(Color.GREEN);
                paintbrush.setPathEffect(dashPath);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(10);


                canvas.drawRect(
                        this.monkey.getHitbox().left - 20,
                        this.monkey.getHitbox().top - 20,
                        monkey.getHitbox().right + 20,
                        monkey.getHitbox().bottom + 20,
                        paintbrush
                );
                DashPathEffect dashPath1 = new DashPathEffect(new float[]{5, 5}, (float) 1.0);
                paintbrush.setPathEffect(dashPath1);
                paintbrush.setColor(Color.RED);
                canvas.drawRect(
                        this.monkey.getHitbox().left ,
                        this.monkey.getHitbox().top ,
                        monkey.getHitbox().right ,
                        monkey.getHitbox().bottom,
                        paintbrush
                );
            }
            if(tripleLive == 1||tripleLive== 4|| tripleLive == 7) {
                dead.start();

                DashPathEffect dashPath = new DashPathEffect(new float[]{5, 5}, (float) 1.0);
                // Paint p = new Paint();
                paintbrush.setColor(Color.RED);
                paintbrush.setPathEffect(dashPath);
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setStrokeWidth(10);


                canvas.drawRect(
                        this.monkey.getHitbox().left - 20,
                        this.monkey.getHitbox().top - 20,
                        monkey.getHitbox().right + 20,
                        monkey.getHitbox().bottom + 20,
                        paintbrush
                );
            }
//            if(this.lives == 0){
//                paintbrush.setStrokeWidth(0);
//                paintbrush.setColor(Color.TRANSPARENT);
//                canvas.drawRect(this.monkey.getHitbox().left-20,this.monkey.getHitbox().top-20,monkey.getHitbox().right+20,monkey.getHitbox().bottom+20,paintbrush);
//            }
            DashPathEffect dashPath = new DashPathEffect(new float[]{0, 0}, (float) 0.0);
            paintbrush.setTextSize(80);     // set font size
            paintbrush.setStrokeWidth(5);  // make text narrow
            paintbrush.setColor(Color.BLUE);
            paintbrush.setPathEffect(dashPath);
            canvas.drawText("Lives: " + this.lives, 50, 100, paintbrush);
            canvas.drawText("Score: " + this.score, 700, 100, paintbrush);

            if (gameOver == true) {
                paintbrush.setTextSize(120);     // set font size
                // paintbrush.setStrokeWidth(20);  // make text narrow
                paintbrush.setColor(Color.BLUE);
                canvas.drawText("Lives: " + this.lives, 50, 100, paintbrush);
                canvas.drawText("GAME OVER!", 270, 400, paintbrush);
                mysong.release();
                try {
                    gameThread.sleep(1000);

                }
                catch (InterruptedException e) {

                }

              // myIntent.putExtra("score", score);
                myIntent.putExtra("lives", "l");
                myIntent.setClass(c, ResultActivity.class);
                c.startActivity(myIntent);

            }
            if(this.time == true){
                canvas.drawBitmap(this.bomb,800,screenHeight- 420, paintbrush);

                canvas.drawRect(this.bombHitBox.left,this.bombHitBox.top, this.bombHitBox.right,this.bombHitBox.bottom, paintbrush);
            }
            if(this.youWin == true || this.score >= 50){
                mysong.release();
                paintbrush.setTextSize(120);     // set font size
                // paintbrush.setStrokeWidth(20);  // make text narrow
                paintbrush.setColor(Color.BLUE);
                canvas.drawText("GAME OVER!", 270, 400, paintbrush);
                try {
                    gameThread.sleep(1000);

                }
                catch (InterruptedException e) {

                }
                // myIntent.putExtra("score", score);
                myIntent.putExtra("lives", "w");
                myIntent.setClass(c, ResultActivity.class);
                c.startActivity(myIntent);
            }

         //   drawBackground(0);


            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
            this.checkminutes = this.checkminutes + 1;



            if(this.checkminutes >= 1500){
                this.youWin = true;
            }

            if(this.checkminutes == 100|| this.checkminutes == 400){
                this.time = true;
                bombShow.start();
            }

        }
        catch (InterruptedException e) {

        }
    }
    //final MediaPlayer mp =- MediaPlayer.create(this, R.raw....)
    //mp.start();


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                this.deltaX = (int)event.getX();
                this.deltaY = (int)event.getY();

                break;
            case MotionEvent.ACTION_DOWN:
             int x = (int)event.getX();
               this.touch(x);

                break;


        }
        return true;
    }


    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

