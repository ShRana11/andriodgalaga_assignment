package com.example.sukhrana.androidgalaga;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Sprite {
    int xPosition;
    int yPosition;
    Rect Hitbox;
    Bitmap Image;

    public Rect getHitbox() {
        return Hitbox;
    }

    public void setHitbox(Rect hitbox) {
        Hitbox = hitbox;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }



    public  Sprite(Context context, int x, int y){
        this.xPosition = x;
        this.yPosition = y;

        this.Image = BitmapFactory.decodeResource(context.getResources(), R.drawable.banana);

        // this.Image.setWidth(this.Image.getWidth()/2);

        // version 1 - we use static numbers because the enemy is NOT moving.
        this.Hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.Image.getWidth(),
                this.yPosition + this.Image.getHeight()
        );
    }


    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

}

